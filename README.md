# Coral Talk

This project helps integrate the Talk commenting service from Mozilla's Coral
Project.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/coral_talk).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/coral_talk).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following outside of Drupal core:

- [Coral Talk instance](https://coralproject.net/talk/)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Content Authoring > Coral
   Talk Settings to configure Coral Talk commenting.
3. Enter the host domain and select which content types to enable comments
   for.
4. Save configuration.


## Maintainers

- James Nettik - [jnettik](https://www.drupal.org/u/jnettik)

**Supporting organizations:**

- [Aten Design Group](https://www.drupal.org/aten-design-group)

**Note:** Development is not associated with Mozilla or the Coral Project.